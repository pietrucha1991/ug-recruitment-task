package com.nbpApi.NBPApi.setup;

import com.nbpApi.NBPApi.feature.dto.ComputerDto;
import com.nbpApi.NBPApi.feature.model.Computer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Fixture {

    public static List<ComputerDto> prepareComputersForUsdPrice() {
        ComputerDto computerOne = ComputerDto
                .builder()
                .name("computerOne")
                .costUsd(345)
                .build();
        ComputerDto computerTwo = ComputerDto.builder()
                .name("computerTwo")
                .costUsd(543)
                .build();
        ComputerDto computerThree = ComputerDto.builder()
                .name("computerThree")
                .costUsd(346)
                .build();
        return List.of(computerOne, computerTwo, computerThree);
    }

    public static List<Computer> preparePcsForDatabaseSearch() {
        List<Computer> computerList = new ArrayList<>();
        for (int i = 0; i < 5; i++){
            Computer computer = Computer.builder()
                    .cost_USD(300 + (i*12))
                    .accounting_date(LocalDate.of(2022, 1, 3))
                    .name("computer" + i)
                    .build();
            computerList.add(computer);
        }
        return computerList;
    }
}
