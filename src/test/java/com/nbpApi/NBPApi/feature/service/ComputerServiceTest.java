package com.nbpApi.NBPApi.feature.service;

import com.nbpApi.NBPApi.feature.dto.InputDto;
import com.nbpApi.NBPApi.feature.model.Computer;
import com.nbpApi.NBPApi.feature.repository.ComputerRepository;
import com.nbpApi.NBPApi.setup.Fixture;
import com.nbpApi.NBPApi.setup.IntegrationTests;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ComputerServiceTest extends IntegrationTests {

    public static final File INVOICE = new File(System.getProperty("user.dir") + File.separator + "invoice.xml");
    @Autowired
    ComputerService computerService;
    @Autowired
    ComputerRepository computerRepository;


    @AfterEach
    void tearDown() {
        computerRepository.deleteAll();
        INVOICE.delete();
    }

    @Test
    void shouldCountThePriceOfPcWithTheDateOfThirdJanuary() throws IOException {
        //given
        InputDto inputDto = InputDto.builder()
                .computerList(Fixture.prepareComputersForUsdPrice())
                .purchaseDto(LocalDate.of(2022, 1, 3))
                .build();

        //when
        BigDecimal totalPriceOfPcsInPLN = computerService.countTotalPriceOfComputersInPln(inputDto);

        //then
        assertThat(totalPriceOfPcsInPLN.doubleValue()).isEqualTo(5060.15);
        assertTrue(INVOICE.exists());
        assertEquals(inputDto.getComputerList().size(), computerRepository.findAll().size());
    }

    @Test
    void shouldCountThePriceOfPcWithTheDateOfTenthJanuary() throws IOException {
        //given
        InputDto inputDto = InputDto.builder()
                .computerList(Fixture.prepareComputersForUsdPrice())
                .purchaseDto(LocalDate.of(2022, 1, 10))
                .build();

        //when
        BigDecimal totalPriceOfPcsInPLN = computerService.countTotalPriceOfComputersInPln(inputDto);

        //then
        assertThat(totalPriceOfPcsInPLN.doubleValue()).isEqualTo(5002.02);
        assertTrue(INVOICE.exists());
        assertEquals(inputDto.getComputerList().size(), computerRepository.findAll().size());
    }


    @Test
    void shouldFindPcByNameAndDate() {
        //given
        List<Computer> computerList = Fixture.preparePcsForDatabaseSearch();
        computerRepository.saveAll(computerList);
        //when
        List<Computer> foundPcList = computerService.findByNameContainsAndDate("3", LocalDate.of(2022, 1, 3));
        //then
        assertThat(foundPcList).hasSize(1);
    }
}
