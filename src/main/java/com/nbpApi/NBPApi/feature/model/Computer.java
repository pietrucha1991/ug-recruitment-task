package com.nbpApi.NBPApi.feature.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.bytebuddy.asm.Advice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Computer {

    @Id
    @GeneratedValue
    public Long computerId;
    @Column(name = "nazwa")
    public String name;
    @Column(name = "data_ksiegowania")
    public LocalDate accounting_date;
    @Column(name = "koszt_USD")
    public double cost_USD;
    @Column(name = "koszt_PLN")
    public double cost_PLN;
}
