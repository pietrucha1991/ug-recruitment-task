package com.nbpApi.NBPApi.feature.repository;

import com.nbpApi.NBPApi.feature.model.Computer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface ComputerRepository extends JpaRepository<Computer, Long> {

    @Query(value = "SELECT * FROM computer WHERE computer.NAZWA LIKE %:namePart% AND computer.data_ksiegowania = :accountingDate", nativeQuery = true)
    List<Computer> findByNameContainsAndAccounting_date(String namePart, LocalDate accountingDate);
}
