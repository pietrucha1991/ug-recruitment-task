package com.nbpApi.NBPApi.feature.controller;

import com.nbpApi.NBPApi.feature.dto.InputDto;
import com.nbpApi.NBPApi.feature.model.Computer;
import com.nbpApi.NBPApi.feature.service.ComputerService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("nbpApi")
@RequiredArgsConstructor
public class ComputerController {

    private final ComputerService computerService;

    @PostMapping
    public BigDecimal countTotalPriceOfComputers(@RequestBody InputDto inputDto) throws IOException {
        return computerService.countTotalPriceOfComputersInPln(inputDto);
    }

    @GetMapping("/find")
    public List<Computer> findByNameContains(@RequestParam("partName") String partName,
                                             @RequestParam("accountingDate")
                                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate accountingDate) {
        return computerService.findByNameContainsAndDate(partName, accountingDate);
    }

}
