package com.nbpApi.NBPApi.feature.service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nbpApi.NBPApi.feature.dto.ComputerDto;
import com.nbpApi.NBPApi.feature.dto.ComputerXmlDto;
import com.nbpApi.NBPApi.feature.dto.InputDto;
import com.nbpApi.NBPApi.feature.dto.InvoiceDto;
import com.nbpApi.NBPApi.feature.model.Computer;
import com.nbpApi.NBPApi.feature.model.CurrencyTable;
import com.nbpApi.NBPApi.feature.repository.ComputerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ComputerService {

    private static final String URL_TEMPLATE = "http://api.nbp.pl/api/exchangerates/rates/c/usd/%s?format=json";
    private static final XmlMapper XML_MAPPER = new XmlMapper();
    private final ComputerRepository computerRepository;
    private final RestTemplate restTemplate;

    public BigDecimal countTotalPriceOfComputersInPln(InputDto input) throws IOException {
        List<Computer> computers = storeComputers(input);

        double totalPriceInPln = computers.stream().mapToDouble(Computer::getCost_PLN).sum();

        printXml(computers);

        return BigDecimal.valueOf(totalPriceInPln).setScale(2, RoundingMode.CEILING);
    }

    private void printXml(List<Computer> computerList) throws IOException {
        List<ComputerXmlDto> xmlComputerList = ComputerXmlDto.ofList(computerList);

        InvoiceDto invoiceDto = InvoiceDto.builder().computerList(xmlComputerList).build();

        XML_MAPPER.writeValue(new File("invoice.xml"), invoiceDto);
    }

    private List<Computer> storeComputers(InputDto inputDto) {
        double exchangeRate = getExchangeRate(inputDto.getPurchaseDto());
        List<Computer> computers = new ArrayList<>();

        for (ComputerDto computerDto : inputDto.getComputerList()) {
            double computerPriceInPLN = exchangeRate * computerDto.getCostUsd();

            Computer computer = Computer.builder()
                    .name(computerDto.getName())
                    .accounting_date(inputDto.getPurchaseDto())
                    .cost_USD(computerDto.getCostUsd())
                    .cost_PLN(computerPriceInPLN)
                    .build();

            computers.add(computer);
        }
        return computerRepository.saveAll(computers);
    }

    private double getExchangeRate(LocalDate usdPriceDate) {
        String targetUrl = String.format(URL_TEMPLATE, usdPriceDate);
        return restTemplate.getForObject(targetUrl, CurrencyTable.class).getRates().get(0).getAsk();
    }

    public List<Computer> findByNameContainsAndDate(String partName, LocalDate accountingDate) {
        return computerRepository.findByNameContainsAndAccounting_date(partName, accountingDate);
    }
}
