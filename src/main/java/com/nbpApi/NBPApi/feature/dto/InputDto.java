package com.nbpApi.NBPApi.feature.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InputDto {
    private LocalDate purchaseDto;
    private List<ComputerDto> computerList;
}
