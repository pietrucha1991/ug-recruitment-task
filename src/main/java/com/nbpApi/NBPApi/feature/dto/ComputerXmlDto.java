package com.nbpApi.NBPApi.feature.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.nbpApi.NBPApi.feature.model.Computer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ComputerXmlDto {
    @JacksonXmlProperty(localName = "nazwa")
    private String name;
    @JacksonXmlProperty(localName = "data_księgowania")
    private String accountingDate;
    @JacksonXmlProperty(localName = "koszt_USD")
    private double usdPrice;
    @JacksonXmlProperty(localName = "koszt_PLN")
    private double plnPrice;

    public static ComputerXmlDto of (Computer computerModel){
        String date = computerModel.getAccounting_date().toString();

        return ComputerXmlDto.builder()
                .name(computerModel.getName())
                .usdPrice(computerModel.getCost_USD())
                .plnPrice(computerModel.getCost_PLN())
                .accountingDate(date)
                .build();
    }

    public static List<ComputerXmlDto> ofList(List<Computer> computerModelList){
        List<ComputerXmlDto> computerXmlDtos = new ArrayList<>();

        for(Computer computerModel : computerModelList){
            computerXmlDtos.add(ComputerXmlDto.of(computerModel));
        }

        return computerXmlDtos;
    }

}
