# UG Recruitment Task
## ZADANIE:

Zadanie polega na przygotowaniu aplikacji, 
która przeliczy kwotę z USD na PLN w oparciu o API NPB na dany dzień, 
zapisze do pliku XML oraz bazy danych i następnie wyświetli dane z 
zapisanej bazy danych w odpowiednim formacie.

## Opis działania:

Firma kupiła 3 komputery za kwotę 1234 USD.
- komputer 1 – kwota 345 USD
- komputer 2 – kwota 543 USD
- komputer 3 – kwota 346 USD

Należy napisać aplikację, która skorzysta ze strony NBP 
(użyje JSON i odpyta serwis NBP pod adresem: http://api.nbp.pl), 
gdzie parametrem będzie data przewalutowania i przeliczy koszt zakupu komputerów na PLN
(dokumentacja i API pod adresem http://api.nbp.pl).

Po przeliczeniu wynik zostanie zwrócony do użytkownika i
dane zostaną zapisane w bazie danych, oraz w pliku XML

Format pliku XML:
<faktura>
  <komputer>
      <nazwa></nazwa>
      <data_ksiegowania></data_ksiegowania>
      <koszt_USD></koszt_USD>
      <koszt_PLN></koszt_PLN>
  </komputer>
  <komputer>
      <nazwa></nazwa>
      <data_ksiegowania></data_ksiegowania>
      <koszt_USD></koszt_USD>
      <koszt_PLN></koszt_PLN>
  </komputer>
</faktura>

Tabela w bazie danych:

nazwa | data_ksiegowania | koszt_USD | koszt_PLN

Po zapisie do bazy danych aplikacja powinna móc wyszukać komputery po
nazwie (wpisując tylko fragment nazwy komputera) oraz po dacie księgowania. 
Należy wyświetlić wszystkie dane o komputerze 
oraz mieć możliwość sortowania po nazwie oraz dacie księgowania.
Komputery należy zapisać z datą przewalutowania 3 stycznia 2022 i 10 
stycznia 2022.

# Application-properties
W zadaniu użyłem MySql, wiec application-properties należy ustawić pod swoją 
lokalną bazę danych.

# Testowanie
Aby przetestować aplikację, należy wejść w poniższy link:
http://localhost:8080/swagger-ui/index.html
